
.text
.globl drawLine
.globl drawCurve


# Draws polygon
# $a0 - address of buffer containing polygon points
# $a1 - count of coordinates
# $a2 - address of grayscale pixel buffer
# $a3 - width of the pixel buffer
drawCurve:
					# --- PROLOGUE ---
		subiu	$sp, $sp, 28	# 28 (preserve ($ra, $s0, $s1, $s2, $s3, $s4, $s5)
		sw	$ra, ($sp)	# preserve $ra
		sw	$s0, 4($sp)	# preserve $s0
		sw	$s1, 8($sp)	# preserve $s1
		sw	$s2, 12($sp)	# preserve $s2
		sw	$s3, 16($sp)	# preserve $s3
		sw	$s4, 20($sp)	# preserve $s4
		sw	$s5, 24($sp)	# preserve $s5
		move	$s0, $a0	# $s0 - address of buffer containing polygon points
		move	$s1, $a1	# $s1 - count of points, will be used as counter
		move	$s2, $a2	# $s2 - address of grayscale pixel buffer
		move	$s3, $a3	# $s3 - width of the pixel buffer
		lw	$s4, ($s0)	# $s4 - first x coordinate
		lw	$s5, 4($s0)	# $s5 - first y coordinate
		
		subiu	$s1, $s1, 1	# first point read, decrement counter
		addiu	$s0, $s0, 8	# next coordinates address
		
		move	$a0, $s4	# x1
		move	$a1, $s5	# y1
				
drawCurve_l:	lw	$a2, ($s0)	# x2
		lw	$a3, 4($s0)	# y2
		subiu	$sp, $sp, 8	# make space for arguments
		sw	$s2, 4($sp)	# address of pixel buffer
		sw	$s3, ($sp)	# width of pixel buffer
		jal	drawLine	# draws line
		addiu	$sp, $sp, 8	# cleanup arguments from the stack
		lw	$a0, ($s0)	# x1 argument for next loop
		lw	$a1, 4($s0)	# y1 argument for next loop
		addiu	$s0, $s0, 8	# next coordinates address for next loop
		
		subiu	$s1, $s1, 1	# decrement points counter
		bgtz	$s1, drawCurve_l # jump if there are remaining lines to draw
		
					# --- EPILOGUE ---
		lw	$ra, ($sp)	# restore $ra
		lw	$s0, 4($sp)	# restore $s0
		lw	$s1, 8($sp)	# restore $s1
		lw	$s2, 12($sp)	# restore $s2
		lw	$s3, 16($sp)	# restore $s3
		lw	$s4, 20($sp)	# restore $s4
		lw	$s5, 24($sp)	# restore $s5
		addiu	$sp, $sp, 28	# cleanup stack
		
		jr	$ra

# Draws line between specified points
# Uses Bresenham's algorithm
# $a0 - x start
# $a1 - y start
# $a2 - x end
# $a3 - y end
# 8($fp) - address of grayscale pixel buffer
# 4($fp) - width of the bitmap
drawLine:
					# --- PROLOGUE ---
		subiu	$sp, $sp, 4	# 4 (preserve $fp)
		sw	$fp, ($sp)	# preserve $fp
		move	$fp, $sp	# set up frame pointer
		
		subiu	$sp, $sp, 4	# 4 (preserve $ra)
		sw	$ra, ($sp)	# preserve $ra

		lw	$t8, 8($fp)	# $t8 - address of grayscale pixel buffer
		lw	$t9, 4($fp)	# $t9 - width of the bitmap
	
					# --- COMPUTE INITIAL VALUES ---
		move	$t0, $a0	# $t0 - current x
		move	$t1, $a1	# $t1 - current y
		
		subu	$t2, $a2, $a0	# $t2 - dx, distance between x end and x start
		bgez	$t2, drawLine_nodx # jump if $t2 >= 0
		negu	$t2, $t2	# absolute value of dx	
		
drawLine_nodx:	sll	$t2, $t2, 1	# dx *= 2, to avoid round error while dividing by 2 later
		subu	$t3, $a3, $a1	# $t3 - dy, distance between y end and y start
		bgez	$t3, drawLine_nody # jump if $t3 >=0
		negu	$t3, $t3	# absolute value of dy
			
drawLine_nody:	sll	$t3, $t3, 1	# dy *= 2, to avoid round error while dividing by 2 later
		li	$t6, 1		# $t6 - x step from x start to x end
		ble	$a0, $a2, drawLine_posx # do not negate if x start is less or equal x end
		negu	$t6, $t6	# $t6 = -1
		
drawLine_posx:	li	$t7, 1		# $t7 - y step from y start to y end
		ble	$a1, $a3, drawLine_posy	# do not negate if y start is less or equal y end
		negu	$t7, $t7	# $t7 = -1
		
drawLine_posy:						
					# --- SET ARGUMENTS FOR setPixel ---
					# --- WILL BE USED ACROSS ALL CALLS ---
					# --- setPixel affects only $a3 ---
		move	$a0, $t8	# address of buffer
		move	$a1, $t9	# width of the pixel buffer
		
					# --- DRAW BEGIN PIXEL
		move	$a2, $t0	# x coord
		move	$a3, $t1	# y coord
		jal	setPixel	# set pixel
		
		blt	$t2, $t3, drawLine_angle # jump if angle is > 45 degree
		
					# --- ANGLE <= 45 degree ---
		sra	$t4, $t2, 1	# $t4 - error, initial = dx / 2
		sra	$t5, $t2, 1	# $t5 - steps counter, by dx / 2 (because it's multiplied by 2)
drawLine_xloop:	addu	$t0, $t0, $t6	# next x
		subu	$t4, $t4, $t3	# modify error with dy
		
		bgez	$t4, drawLine_xsp # jump if error is >= 0
		
		addu	$t1, $t1, $t7	# next y
		addu	$t4, $t4, $t2	# modify error with dx
		
drawLine_xsp:	move	$a2, $t0	# x coord
		move	$a3, $t1	# y coord
		jal	setPixel	# set pixel
		
		subiu	$t5, $t5, 1	# decrement counter
		bgtz	$t5, drawLine_xloop # loop if not finished
		b	drawLine_end	# finish
		
					# --- ANGLE > 45 degree --
drawLine_angle:	sra	$t4, $t3, 1	# $t4 - error, initial = dy / 2
		sra	$t5, $t3, 1	# $t5 - steps counter, by dy / 2 (because it's multiplied by 2)	
drawLine_yloop:	addu	$t1, $t1, $t7	# next y
		subu	$t4, $t4, $t2	# modify error with dx
		
		bgez	$t4, drawLine_ysp # jump if error is >= 0
		addu	$t0, $t0, $t6	# next x
		addu	$t4, $t4, $t3	# modify error with dy
		
drawLine_ysp:	move	$a2, $t0	# x coord
		move	$a3, $t1	# y coord
		jal	setPixel
		
		subiu	$t5, $t5, 1	# decrement counter
		bgtz	$t5, drawLine_yloop # loop if not finished

					# --- EPILOGUE ---
drawLine_end:	lw	$ra, ($sp)	# restore $ra
		lw	$fp, ($fp)	# restore $fp
		addiu	$sp, $sp, 8	# cleanup stack
		
		jr	$ra

# Sets pixel at specified coordinates to black color (0x00)
# $a0 - address of grayscale pixel buffer
# $a1 - width of pixel buffer
# $a2 - x coordinate
# $a3 - y coordinate
# Remarks: does not modify any register except $a2 and $a3
setPixel:
		mul	$a3, $a3, $a1	# compute row offset
		addu	$a3, $a3, $a2	# x offset
		addu	$a3, $a3, $a0	# add address of buffer to computed offset
		li	$a2, 0xFF	# white color
		sb	$a2, ($a3)	# store white color at specified address
		
		jr	$ra

