
.text
.globl loadData
.globl findDigMin
.globl findChar

# Loads data from file
# $a0 - address of null-terminated string containing file name
# $a1 - address of array where coordinates will be stored, structure: [x_0, y_0, x_1, y_1, ... , x_n, y_n], elements are words
# Returns:
# $v0 - count of steps
# $v1 - number of coordinates read
loadData:
					# --- PROLOGUE ---
		subiu	$sp, $sp, 280	# 24 (preserve $ra, $s0, $s1, $s2, $s3, $s4) + 256 (byte buffer)
		sw	$ra, 256($sp)	# preserve $ra
		sw	$s0, 260($sp)	# preserve $s0
		sw	$s1, 264($sp)	# preserve $s1
		sw	$s2, 268($sp)	# preserve $s2
		sw	$s3, 272($sp)	# preserve $s3
		sw	$s4, 276($sp)	# preserve $s4
					# $s0 - file descriptor (set after opening file)
		move	$s1, $a1	# $s1 - address of array to store coordinates
					# $s2 - count of steps (set after reading first line)
					# $s3 - current buffer address (used for parsing data in memory buffer)
					# $s4 - count of coordinates (counter for coordinates, increments every coordinate read)
					
					# --- OPENING FILE ---
		li	$v0, 13		# open file service
		# $a0			# file descriptor, already set
		li	$a1, 0		# read-only flag
		# $a2			# $a2 contains mode but MARS ignores it
		syscall			# open file
		move	$s0, $v0	# set $s0 to file descriptor
		
					# --- READING FILE ---
		li	$v0, 14		# read from file service
		move	$a0, $s0	# file descriptor
		la	$a1, ($sp)	# buffer
		li	$a2, 255	# maximum characters to read
		syscall			# read file
					# $v0 contains number of bytes read
		la	$t0, ($sp)	# read file does not put '\0' on the end of buffer
		addu	$t0, $t0, $v0	# so we have to set it manually
		sb	$zero, ($t0)	# to indicate end of buffer
		
					# --- CLOSING FILE ---
		li	$v0, 16		# close file service
		move	$a0, $a0	# file descriptor
		syscall			# close file
		
					# --- PARSE COUNT OF STEPS ---
		la	$s3, ($sp)	# $s3 holds buffer address now
		
		move	$a0, $s3	# address of string to search
		jal 	findDigMin	# search string for digit or minus sign
		move	$s3, $v0	# $v0 contains address where number starts
		
		move	$a0, $s3	# address of string to search
		jal 	findEndl	# search string for end of line
		sb	$zero, ($v0)	# $v0 contains address of new line char, replace with zero to indicate end of number
		
		move	$a0, $s3	# address of null-terminated string to pass for strToWord
		addiu	$s3, $v0, 1	# set $s3 to address that points to the next character after current string
		jal	strToWord	# convert string into word
		move	$s2, $v0	# $v0 contains converted number, store it in $s2
		
					# --- PARSE COORDINATES ---
		li	$s4, 0		# set count of coordinates to 0
		
					# --- PARSE X ---
loadData_pcrd:	move	$a0, $s3	# address of string to search
		jal	findDigMin	# search string for digit or minus sign
		beqz	$v0, loadData_end # finish if digit/minus sign not found - it means we finished parsing buffer and there is no more numbers
		move	$s3, $v0	# $v0 contains address where number starts
		
		move	$a0, $s3	# address of string to search
		li	$a1, ' '	# character to be found
		jal	findChar	# search string for space
		sb	$zero, ($v0)	# $v0 contains address of space, replace with zero to indicate end of number	
		move	$a0, $s3	# address of null-terminated string to pass for strToWord
		addiu	$s3, $v0, 1	# set $s3 to address that points to the next character after current string
		jal	strToWord	# convert string into word
		sw	$v0, ($s1)	# store x coordinate in buffer
		addiu	$s1, $s1, 4	# set buffer address to next free space
		
					# --- PARSE Y ---
		move	$a0, $s3	# addres of string to search
		jal	findDigMin	# search string for digit or minus sign
		move	$s3, $v0	# $v0 contains address where number starts
		
		move	$a0, $s3	# addres of string to search
		jal	findEndl	# search string for end of line
		move	$a0, $s3	# address of null-terminated string to pass for strToWord
		bnez	$v0, loadData_neof # if $v0 == 0 then endl not found - it means end of file
		li	$s3, 0		# end of file occurred, set buffer pointer to 0 to indicate it	
		b	loadData_py	# jump over setting $s3 pointer and null-terminating
loadData_neof:	sb	$zero, ($v0)	# $v0 contains address of new line char, replace with zero to indicate end of number
		addiu	$s3, $v0, 1	# set $s3 to address that points to the next character after current string
loadData_py:	jal	strToWord	# convert string into word
		sw	$v0, ($s1)	# store y coordinate in buffer
		addiu	$s1, $s1, 4	# set buffer address to next free space
		
		addiu	$s4, $s4, 1	# increment number of coordinates
		bnez	$s3, loadData_pcrd # branch to parse next coordinate if not end of file
		
					# --- EPILOGUE ---
loadData_end:	move 	$v0, $s2	# return count of steps
		move	$v1, $s4	# return count of read coordinates
					
		lw	$ra, 256($sp)	# restore $ra
		lw	$s0, 260($sp)	# restore $s0
		lw	$s1, 264($sp)	# restore $s1
		lw	$s2, 268($sp)	# restore $s2
		lw	$s3, 272($sp)	# restore $s3
		lw	$s4, 276($sp)	# restore $s4
		addiu	$sp, $sp, 280	# cleanup stack
	
		jr $ra

# Converts null-terminated string to integer
# $a0 - address of null-terminated string containing number to parse
# Returns:
# $v0 - parsed number
strToWord:				
					# --- CHECK FOR NEGATIVE ---
		li	$t2, 1		# $t2 - multiplier used later for conversion
		lb	$t1, ($a0)	# load first character to check if number is negative (if has minus sign on the beginning)	
		bne	$t1, '-', strToWord_pos
		addiu	$a0, $a0, 1	# move pointer to next character to avoid processing minus sign
		negu	$t2, $t2	# negate multiplier
		
					# --- COUNT CHARACETERS ---
strToWord_pos:	li	$t0, 0		# $t0 will be used as counter		
strToWord_cnt:	lb	$t1, ($a0)
		beqz	$t1, strToWord_cvt
		addiu	$a0, $a0, 1	# next character address
		addiu	$t0, $t0, 1	# increment counter
		b	strToWord_cnt
		
					# $a0 points now to null-terminator
					
					# --- CONVERT TO INTEGER ---
strToWord_cvt:	li	$v0, 0		# $v0 - result
					# beginning multiplier is already stored in $t2
		
strToWord_ccvt:	subiu	$a0, $a0, 1	# move to previous character
		
		lb	$t1, ($a0)
		subiu	$t1, $t1, '0'	# convert ascii digit to value
		mul	$t1, $t1, $t2	# multiply current digit by multiplier
		addu	$v0, $v0, $t1	# add to result
		
		mul	$t2, $t2, 10	# next multiplier
		
		subiu	$t0, $t0, 1	# decrement characters counter
		bnez	$t0, strToWord_ccvt

		jr 	$ra

# Looks for a digit or minus sign in given string
# $a0 - address of null-terminated string to search
# Returns:
# $v0 - address of found digit in string. if not found returns 0
findDigMin:
		lb 	$t0, ($a0)	# load character into $t0
		bge 	$t0, '0', findDigMin_g0
					
					# $t0 < '0'
		beq	$t0, '-', findDigMin_fnd
		addiu	$a0, $a0, 1	# next character
		bnez	$t0, findDigMin
					
					# $t0 == '\0'
		li	$v0, 0		# null-terminator found
		jr 	$ra
		
findDigMin_g0:				# $t0 >= '0'
		ble	$t0, '9', findDigMin_fnd
		
					# $t0 > '9'
		addiu	$a0, $a0, 1	# next character
		b	findDigMin
		
findDigMin_fnd:	move	$v0, $a0
		jr	$ra
		
# Looks for specified character in given string
# $a0 - address of null-terminated string to search
# $a1 - character to find
# Returns:
# $v0 - address of found character in string. if not found returns 0
findChar:
		lb	$t0, ($a0)	# load character into $t0
		beq	$t0, $a1, findChar_fnd # jump if equal to predicate
		bnez	$t0, findChar_noz
		
					# $t0 == '\0'
		li	$v0, 0
		jr	$ra		# '\0' found, return with 0
		
findChar_noz:	addiu	$a0, $a0, 1	# next character
		b	findChar	# continue

findChar_fnd:	move	$v0, $a0	# put found address into $v0
		jr	$ra
		
# Looks for CR or LF in given string
# $a0 - address of null-terminated string to search
# Returns:
# $v0 - address of found CR or LF in string. if not found returns 0
findEndl:
		lb	$t0, ($a0)	# load character into $t0
		beq	$t0, '\r', findEndl_fnd # equal to predicate
		beq	$t0, '\n', findEndl_fnd
		bnez	$t0, findEndl_noz
		
					# $t0 == '\0'
		li	$v0, 0
		jr	$ra		# '\0' found, return with 0
		
findEndl_noz:	addiu	$a0, $a0, 1	# next character
		b	findEndl	# continue

findEndl_fnd:	move	$v0, $a0	# put found address into $v0
		jr	$ra		
