
.data
colorTable: .word 0x00FFFFFF, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0

.text
.globl saveBmp

# Constructs and saves bitmap	
# $a0 - address of null-terminated string containing file name
# $a1 - address of grayscale pixel buffer of size width * height bytes (1 byte = 1 pixel)
# $a2 - width of bitmap
# $a3 - height of bitmap
saveBmp:
					# --- PROLOGUE ---
		subiu	$sp, $sp, 88	# 24 (preserve $s0, $s1, $s2, $s3, $s4, $s5) + 64 (byte buffer)
		sw 	$s0, 64($sp)	# preserve $s0
		sw	$s1, 68($sp)	# preserve $s1
		sw	$s2, 72($sp)	# preserve $s2
		sw	$s3, 76($sp)	# preserve $s3
		sw	$s4, 80($sp)	# preserve $s4
		sw	$s5, 84($sp)	# preserve $s5
					# $s0 - file descriptor (set after opening file)
		move 	$s1, $a1	# $s1 - grayscale pixel buffer to save
		move 	$s2, $a2	# $s2 - width of bitmap
		move 	$s3, $a3	# $s3 - height of bitmap
					# $s4 - row padding (set after computing raw bitmap data size)
					# $s5 - row size in bytes (set after computing raw bitmap data size)
		
					# --- OPENING FILE ---
		li 	$v0, 13		# open file service
		# $a0			# file name, $a0 is already set to address to file name
		li 	$a1, 1		# write-only with create
		# $a2			# $a2 contains mode but MARS ignores it
		syscall			# open file
		move 	$s0, $v0	# set $s0 to file descriptor
		
					# --- COMPUTE RAW BITMAP DATA SIZE ---
		#move	$s5, $s2	# store row size in bytes in $s5
		move	$t7, $s2	# store in $t7 bitmap width
		and	$t0, $t7, 0x3   # find remainder
		li	$s4, 0
		beqz	$t0, saveBmp_nopad
		li	$s4, 4
		subu	$s4, $s4, $t0   # compute padding
		addu	$t7, $t7, $s4	# add row padding to row size
saveBmp_nopad:	mul	$t7, $t7, $s3	# row size * bitmap height = total raw bitmap data size
					# $s4 stores required row padding
					# $s5 stores row size in bytes
					# $t7 stores total raw size
		
					# --- WRITE BITMAP HEADER TO THE BUFFER ---
		li	$t0, 'B'	# BM
		sb	$t0, ($sp)
		li 	$t0, 'M'
		sb	$t0, 1($sp)
		
		addiu	$t0, $t7, 1078	# bitmap size
		sh	$t0, 2($sp)	# total bitmap size = raw data size + 54 (headers size) + 1024 (color table size)
		srl	$t0, $t0, 16
		sh	$t0, 4($sp)
			
		sh	$zero, 6($sp)	# 2 * 2 bytes reserved fields
		sh	$zero, 8($sp)
		
		li	$t0, 1078	# offset where pixel array starts = 14 (header) + 40 (DIB header - BITMAPINFOHEADER) + 1024 (color table)
		sh	$t0, 10($sp)
		srl	$t0, $t0, 16
		sh	$t0, 12($sp)
		
					# --- WRITE DIB HEADER TO THE BUFFER ---
		li	$t0, 40		# DIB header size
		sh 	$t0, 14($sp)
		srl	$t0, $t0, 16
		sh 	$t0, 16($sp)
		
		move	$t0, $s2	# bitmap width
		sh 	$t0, 18($sp)
		srl	$t0, $t0, 16
		sh 	$t0, 20($sp)
		
		move	$t0, $s3	# bitmap height
		sh	$t0, 22($sp)
		srl	$t0, $t0, 16
		sh	$t0, 24($sp)
		
		li	$t0, 1		# number of color planes
		sh	$t0, 26($sp)
		
		li	$t0, 8		# bits per pixel
		sh	$t0, 28($sp)
			
		sh 	$zero, 30($sp)	# compression
		srl	$zero, $t0, 16
		sh	$zero, 32($sp)
					
		sh	$t7, 34($sp) 	# raw bitmap data size
		srl	$t7, $t7, 16	# total raw bitmap data size computed on the beginning was stored in $t7
		sh	$t7, 36($sp)
		
		sh	$zero, 38($sp)	# 4 * 4 zeroed fields (no need to use in this program)
		sw	$zero, 40($sp)
		sw	$zero, 44($sp)
		sw	$zero, 48($sp)
		sh	$zero, 52($sp)
		
					# --- WRITE BUFFER WITH ENTIRE HEADER TO FILE ---
		li	$v0, 15		# write to file service
		move	$a0, $s0	# file descriptor
		la	$a1, ($sp)	# buffer
		li	$a2, 54		# bytes to write
		syscall			# save bitmap header to file
		
					# --- WRITE COLOR TABLE ---
		li	$v0, 15		# write to file service
		move	$a0, $s0	# file descriptor
		la	$a1, colorTable # buffer
		li	$a2, 1024	# bytes to write
		syscall			# save color table to fil					
		
					# --- WRITE PIXELS ---
					# $s3 stores bitmap height, now it will be used as row counter
		sw	$zero, ($sp)	# store 4-byte zero at beginning of the buffer for writing padding
		
saveBmp_wrow:	li	$v0, 15		# write to file service
		move	$a0, $s0	# file descriptor
		move	$a1, $s1	# pixel buffer
		move	$a2, $s2	# pixels to write
		syscall			# save pixels
		
		beqz	$s4, saveBmp_wnopad # if no padding then skip step which writes padding
		li	$v0, 15		# write to file service
		move	$a0, $s0	# file descriptor
		la	$a1, ($sp)	# buffer which contains 4-byte zero
		move	$a2, $s4	# bytes to write, count of padding bytes
		syscall			# save padding
		
saveBmp_wnopad:	subiu	$s3, $s3, 1	# decrement remaining row count
		addu	$s1, $s1, $s2	# next row in pixel buffer
		bnez	$s3, saveBmp_wrow # continue to next row if exists
		
					# --- CLOSING FILE ---
		li	$v0, 16		# close file service
		move 	$a0, $s0	# file descriptor
		syscall			# close file
		
					# --- EPILOGUE ---
		lw	$s0, 64($sp)	# restore $s0
		lw	$s1, 68($sp)	# restore $s1
		lw	$s2, 72($sp)	# restore $s2
		lw	$s3, 76($sp)	# restore $s3
		lw	$s4, 80($sp)	# restore $s4
		lw	$s5, 84($sp)	# restore $s5
		addiu 	$sp, $sp, 88	# cleanup stack
		
		jr 	$ra
