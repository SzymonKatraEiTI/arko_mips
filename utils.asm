
.text
.globl findMin
.globl findMax
.globl alignCoords
.globl convertToFP
.globl convertToInt
.globl mulFP
.globl memset

# Finds minimum value (word) in given array.
# $a0 - address of first element
# $a1 - count of elements
# $a2 - distance between elements (in bytes)
# Returns:
# $v0 - minimum value
findMin:	
					# --- FIRST ELEMENT IS MINUMUM ---
		lw	$v0, ($a0)	# $v0 - hold minimum value
		addu	$a0, $a0, $a2	# move to next element
		subiu	$a1, $a1, 1	# decrement counter
		bnez	$a1, findMin_sch # jump to search if there is more than 1 element in array in total
		jr	$ra		# there was only one element, return
		
					# --- SEARCH ARRAY FOR VALUE LESS THAN FIRST ---
findMin_sch:	lw	$t0, ($a0)	# load element into $t0
		bge	$t0, $v0, findMin_not # jump if not less
		move	$v0, $t0	# smaller value found, store it in $v0
findMin_not:	addu	$a0, $a0, $a2	# move to next element
		subiu	$a1, $a1, 1	# decrement counter
		bnez	$a1, findMin_sch # continue searching if there are still elements to process
		
					# $v0 already contains minimum value
		jr	$ra		
	
# Finds maximum value (word) in given array
# $a0 - address of first element
# $a1 - count of elements
# $a2 - distance between elements (in bytes)	
# Returns
# $v0 - maximum value
findMax:
					# --- FIRST ELEMENT IS MINUMUM ---
		lw	$v0, ($a0)	# $v0 - hold maximum value
		addu	$a0, $a0, $a2	# move to next element
		subiu	$a1, $a1, 1	# decrement counter
		bnez	$a1, findMax_sch # jump to search if there is more than 1 element in array in total
		jr	$ra		# there was only one element, return
		
					# --- SEARCH ARRAY FOR VALUE GREATER THAN FIRST ---
findMax_sch:	lw	$t0, ($a0)	# load element into $t0
		ble	$t0, $v0, findMax_not # jump if not greater
		move	$v0, $t0	# bigger value found, store it in $v0
findMax_not:	addu	$a0, $a0, $a2	# move to next element
		subiu	$a1, $a1, 1	# decrement counter
		bnez	$a1, findMax_sch # continue searching if there are still elements to process
	
					# $v0 already contains maximum value
		jr	$ra	

# Moves all points into first quarter (x >= 0, y >=0) of cartesian coordinate system
# Computes bound of plane
# $a0 - address of buffer containing coordinates
# $a1 - count of coordinates
# Returns:
# $v0 - width of plane
# $v1 - height of plane
alignCoords:
					# --- PROLOGUE ---
		subiu	$sp, $sp, 12	# 8 ( preserve $ra + $s0 + $s1)
		sw	$ra, ($sp)	# preserve $ra
		sw	$s0, 4($sp)	# preserve $s0
		sw	$s1, 8($sp)	# preserve $s1
		move	$s0, $a0	# $s0 - address of buffer containing coordinates
		move	$s1, $a1	# $s1 - count of coordinates
		
					# --- FIND MINIMUM X ---
		# $a0			# address of first element, already set
		# $a1			# count of elements, already set
		li	$a2, 8		# distance between elements in the buffer
		jal	findMin		# find minimum x
					# $v0 contains minimum x
					
					# --- ALIGN X COORDINATES ---
		move	$t0, $s0	# $t0 - address of currently processed element
		move	$t1, $s1	# $t1 - counter for elements
		
alignCoords_ax:	lw	$t2, ($t0)	# load current element into $t2
		subu	$t2, $t2, $v0	# align element
		sw	$t2, ($t0)	# store aligned element back into buffer
		addiu	$t0, $t0, 8	# move address to next element
		subiu	$t1, $t1, 1	# decrement counter
		bnez	$t1, alignCoords_ax # loop if there are elements to be processed
		
					# --- FIND MINIMUM Y ---
		addiu	$a0, $s0, 4	# address of first element, move by 4 to point at y coordinate
		move	$a1, $s1	# count of elements
		li	$a2, 8		# distance between elements in the buffer
		jal	findMin		# find minimum y
					# $v0 contains minimum y
					
					# --- ALIGN Y COORDINATES ---
		addiu	$t0, $s0, 4	# $t0 - address of currently processed element
		move	$t1, $s1	# $t1 - counter for elements
		
alignCoords_ay:	lw	$t2, ($t0)	# load current element into $t2
		subu	$t2, $t2, $v0	# align element
		sw	$t2, ($t0)	# store aligned element back into buffer
		addiu	$t0, $t0, 8	# move address to next element
		subiu	$t1, $t1, 1	# decrement counter
		bnez	$t1, alignCoords_ay # loop if there are elements to be processed
		
					# --- FIND HEIGHT ---
		addiu	$a0, $s0, 4	# address of first element
		move	$a1, $s1	# count of elements
		li	$a2, 8		# distance between elements in the buffer
		jal	findMax		# find maximum y
		addiu	$v0, $v0, 1	# place for pixels on boundary of the bitmap
		
		subiu	$sp, $sp, 4	# preserve height temporarily
		sw	$v0, ($sp)	# on the stack
		
					# --- FIND WIDTH ---
		move	$a0, $s0	# address of first element
		move	$a1, $s1	# count of elements
		li	$a2, 8		# distance between elements in the buffer
		jal	findMax		# find maximum x
		addiu	$v0, $v0, 1	# place for pixels on boundary of the bitmap	
		
					# --- EPILOGUE ---
					# $v0 already contains width
		lw	$v1, ($sp)	# restore height from the stack
		addiu	$sp, $sp, 4	# and store it in $v1
		
		lw	$ra, ($sp)	# restore $ra
		lw	$s0, 4($sp)	# restore $s0
		lw	$s1, 8($sp)	# restore $s1
		addiu	$sp, $sp, 12	# cleanup stack
		jr	$ra	
		
# Converts integers to fixed number
# $a0 - addres of buffer containing integers
# $a1 - count of numbers
convertToFP:	
		lw	$t0, ($a0)	# load integer
		sll	$t0, $t0, 16	# shift left to make space for fraction on low bits
		sw	$t0, ($a0)	# store fixed point number
		addiu	$a0, $a0, 4	# next number
		subiu	$a1, $a1, 1	# decrement remaining numbers
		bgtz	$a1, convertToFP # continue if there are remaining numbers to convert
		
		jr	$ra

# Converts fixed numbers to integers. If fraction is >= 0.5 then number will be rounded up.
# $a0 - address of buffer containing integers
# $a1 - count of numbers
convertToInt:
		lw	$t0, ($a0)	# load fixed number
		and	$t1, $t0, 0x8000 # check if contains 1/2 bit
		sra	$t0, $t0, 16	# convert to integer
		beqz	$t1, convertToInt_n # jump over adding 1 if fraction is less than 0.5
		addiu	$t0, $t0, 1	# round up
convertToInt_n:	sw	$t0, ($a0)	# store integer
		addiu	$a0, $a0, 4	# next number
		subiu	$a1, $a1, 1	# decrement remaining numbers
		bgtz	$a1, convertToInt # continue if there are remaining numbers to convert

		jr	$ra
		
# Fixed point multiply
# $a0 - fixed point multiplicand
# $a1 - fixed point multiplier
# Result:
# $v0 - multiplied number
mulFP:
		mul	$v0, $a0, $a1	# multiply numbers
		srl	$v0, $v0, 16	# ignore lowest 16 bit of fraction, set highest 16 bits of $t0 to 0
		mfhi	$t1		# get integer result of multiplication
		sll	$t1, $t1, 16	# shift lowest 16 bit of integer
		or	$v0, $v0, $t1	# combine integer and fraction
		
		jr	$ra

# Sets all bytes in gived buffer to specified value
# $a0 - address of the buffer
# $a1 - size of the buffer
# $a2 - value used to fill buffer
memset:
		sb	$a2, ($a0) 	# store byte at buffer
		addiu	$a0, $a0, 1	# next buffer element
		subiu	$a1, $a1, 1	# decrement counter
		bgtz	$a1, memset	# jump if there are unfilled bytes
		
		jr	$ra	
