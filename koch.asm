
.text
.globl koch
.globl fullKoch

# Combination of few koch's curves. Uses fixed point math. 16 lowest bits are reserved for fraction.
# $a0 - address of buffer containing coordinates of points
# $a1 - count of coordinates
# $a2 - steps of koch algorithm
# $a3 - address of output buffer
# Result:
# $v0 - count of computed points
fullKoch:
					# --- PROLOGUE ---
		subiu	$sp, $sp, 28	# 20 (preserve $fp, $s0, $s1, $s2, $s3, $s4, $s5)
		sw	$ra, ($sp)	# preserve $ra
		sw	$s0, 4($sp)	# preserve $s0
		sw	$s1, 8($sp)	# preserve $s1
		sw	$s2, 12($sp)	# preserve $s2
		sw	$s3, 16($sp)	# preserve $s3
		sw	$s4, 20($sp)	# preserve $s4
		sw	$s5, 24($sp)	# preserve $s5
		lw	$s0, ($a0)	# $s0 - x component of first coordinate
		lw	$s1, 4($a0)	# $s1 - y component of first coordinate
		li	$s2, 0		# $s2 - count of computed points
		move	$s3, $a1	# $s3 - remaining coordinates
		move	$s4, $a2	# $s4 - steps of koch algorithm
		move	$s5, $a0	# $s5 - address of current coordinate
		
		subiu	$s3, $s3, 1	# decrement remaining coordinates
		addiu	$s5, $s5, 8	# next element
		
		move	$t0, $a3	# $t0 - address of output buffer
		
		sw	$s0, ($t0)	# store Ax
		sw	$s1, 4($t0)	# store Ay
		addiu	$s2, $s2, 1	# increment count of computed points
		addiu	$t0, $t0, 8	# next element address
		
		move	$a0, $s0	# Ax
		move	$a1, $s1	# Ay
		
fullKoch_loop:	blez	$s4, fullKoch_no # jump over koch algorithm if no steps to be done
		lw	$a2, ($s5)	# Bx
		lw	$a3, 4($s5)	# By
		subiu	$sp, $sp, 8	# space for arguments
		sw	$s4, 4($sp)	# steps of koch algorithm
		sw	$t0, ($sp)	# address of output buffer
		jal	koch		# koch algorithm
		addiu	$sp, $sp, 8	# cleanup stack from arguments
		move	$t0, $v0	# set next address to output
		addu	$s2, $s2, $v1	# sum computed points
		
fullKoch_no:	lw	$a0, ($s5)	# Ax for next loop
		lw	$a1, 4($s5)	# Ay for next loop
		
		
		
		sw	$a0, ($t0)	# store Ax
		sw	$a1, 4($t0)	# store Ay
		addiu	$s2, $s2, 1	# increment count of computed points
		addiu	$t0, $t0, 8	# next output element address
		addiu	$s5, $s5, 8	# next coordinate
		
		subiu	$s3, $s3, 1	# decrement counter
		bgtz	$s3, fullKoch_loop # loop if there are remaining coordinates
		
					# --- COMPUTE CLOSING CURVE ---
		blez	$s4, fullKoch_no2 # jump over koch algorithm if no steps to be done
		move	$a2, $s0	# Bx - first point
		move	$a3, $s1	# By - first point
		subiu	$sp, $sp, 8	# space for arguments
		sw	$s4, 4($sp)	# steps of koch algorithm
		sw	$t0, ($sp)	# address of output buffer
		jal	koch		# koch algorithm
		addiu	$sp, $sp, 8	# cleanup stack from arguments
		move	$t0, $v0	# set next address to output
		addu	$s2, $s2, $v1	# sum computed points
		
fullKoch_no2:	sw	$s0, ($t0)	# store Bx
		sw	$s1, 4($t0)	# store By
		addiu	$s2, $s2, 1	# increment count of computed points
		
					# --- EPILOGUE ---
		move	$v0, $s2	# result, computed points count
		lw	$ra, ($sp)	# restore $ra
		lw	$s0, 4($sp)	# restore $s0
		lw	$s1, 8($sp)	# restore $s1
		lw	$s2, 12($sp)	# restore $s2
		lw	$s3, 16($sp)	# restore $s3
		lw	$s4, 20($sp)	# restore $s4
		lw	$s5, 24($sp)	# restore $s5
		addiu	$sp, $sp, 28	# cleanup stack
		
		jr	$ra

# Step of koch curve. Uses fixed-point math. 16 lowest bits are reserved for fraction.
# $a0 - Ax
# $a1 - Ay
# $a2 - Bx
# $a3 - By
# 8($fp) - remaining steps
# 4($fp) - current address where to store computed points
# Returns:
# $v0 - current address after computing and storing
# $v1 - count of points stored
koch:
					# --- PROLOGUE ---
		subiu	$sp, $sp, 4	# 4 (preserve $fp)
		sw	$fp, ($sp)	# preserve $fp
		move	$fp, $sp	# set up frame pointer
		
		subiu	$sp, $sp, 32	# 32 (3 variables for vectors + store $a2, $a3)
					# -4($fp) - Px
					# -8($fp) - Py
					# -12($fp) - Qx
					# -16($fp) - Qy
					# -20($fp) - Rx
					# -24($fp) - Ry
		sw	$a2, -28($fp)	# -28($fp) - Bx
		sw	$a3, -32($fp)	# -32($fp) - By
		
		subiu	$sp, $sp, 36	# 36 (preserve $ra, $s0, $s1, $s2, $s3, $s4, $s5, $s6, $s7)
		sw	$ra, ($sp)	# preserve $ra
		sw	$s0, 4($sp)	# preserve $s0
		sw	$s1, 8($sp)	# preserve $s1
		sw	$s2, 12($sp)	# preserve $s2
		sw	$s3, 16($sp)	# preserve $s3
		sw	$s4, 20($sp)	# preserve $s4
		sw	$s5, 24($sp)	# preserve $s5
		sw	$s6, 28($sp)	# preserve $s6
		sw	$s7, 32($sp)	# preserve $s7
		
					# fixed point constants used to compute points:
					# 0x8000 - 1/2
					# 0x5555 - 1/3
					# 0xAAAA - 2/3
					# 0x49E6 - sqrt(3) / 6
					# 
					# Symbols:
					# A [x1, y1]
					# B [x2, y2]
					# U - vector between A and B
					# V - perpendicular vector to U
					#
					#	  Q
					#        /\
					#       /  \
					# A___P/    \R___B
					#
					# P = A + (1/3) * U
					# Q = A + (1/2) * U + (sqrt(3) / 6) * V
					# R = A + (2/3) * U
		
					# --- COMPUTE ORTHOGONAL VECTORS AND INIT VARIABLES---
					# U [$s0, $s1] - vector between points
					# V [$s2, $s3] - perpendicular vector
		subu	$s0, $a2, $a0	# x component of U (Bx - Ax)
		subu	$s1, $a3, $a1	# y component of U (By - Ay)
		subu	$s2, $a1, $a3	# x component of V (Ay - By)
		subu	$s3, $a2, $a0	# y component of V (Bx - Ax)
		move	$s4, $a0	# $s4 - Ax
		move	$s5, $a1	# $s5 - Bx
					# $s6 - temporary x
					# $s7 - temporary y				
					
					# --- COMPUTE P ---
		move	$a0, $s0	# multiplicand Ux
		li	$a1, 0x5555	# multiplier - 1/3
		jal	mulFP		# multiply fixed point
		move	$s6, $v0	# store x component of P in $s6
		
		move	$a0, $s1	# multiplicand Uy
		li	$a1, 0x5555	# multiplier - 1/3
		jal	mulFP		# multiply fixed point
		move	$s7, $v0	# store y component of P in $s7
		
		addu	$s6, $s6, $s4	# add x component of A to P
		addu	$s7, $s7, $s5	# add y component of A to P
		
		sw	$s6, -4($fp)	# store Px in memory
		sw	$s7, -8($fp)	# store Py in memory
		
					# --- COMPUTE Q ---
		move	$a0, $s0	# multiplicand - Ux
		li	$a1, 0x8000	# multiplier - 1/2
		jal	mulFP		# multiply fixed point
		move	$s6, $v0	# store x component of Q in $s6
		
		move	$a0, $s2	# multiplicand - Vx
		li	$a1, 0x49E6	# multiplier - sqrt(3) / 6
		jal	mulFP		# multiply fixed point
		add	$s6, $s6, $v0	# add result to x component of Q
		
		move	$a0, $s1	# multiplicand - Uy
		li	$a1, 0x8000	# multiplier - 1/2
		jal	mulFP		# multiply fixed point
		move	$s7, $v0	# store y component of Q in $s7
		
		move	$a0, $s3	# multiplicand - Vy
		li	$a1, 0x49E6	# multiplier - sqrt(3) / 6
		jal	mulFP		# multiply fixed point
		add	$s7, $s7, $v0	# add result to y component of Q
		
		addu	$s6, $s6, $s4	# add x component of A to Q
		addu	$s7, $s7, $s5	# add y component of A to Q
		
		sw	$s6, -12($fp)	# store Qx in memory
		sw	$s7, -16($fp)	# store Qy in memory
		
					# --- COMPUTE R ---
		move	$a0, $s0	# multiplicand - Ux
		li	$a1, 0xAAAA	# multiplier - 2/3
		jal	mulFP		# multiply fixed point
		move	$s6, $v0	# store x component of R in $s6
		
		move	$a0, $s1	# multiplicand - Uy
		li	$a1, 0xAAAA	# multiplier - 2/3
		jal	mulFP		# multiply fixed point
		move	$s7, $v0	# store y component of R in $s7
		
		addu	$s6, $s6, $s4	# add x component of A to R
		addu	$s7, $s7, $s5	# add y component of A to R
		
		sw	$s6, -20($fp)	# store Rx in memory
		sw	$s7, -24($fp)	# store Ry in memory
		
					# --- INIT VARIABLES FOR RECURSION ---
		lw	$s0, 4($fp)	# $s0 - current address
		lw	$s1, 8($fp)	# $s1 - remaining steps
		li	$s2, 0		# $s2 - count of added points
					# $s4 - Ax, already set
					# $s5 - Ay, already set
		
		subiu	$s1, $s1, 1	# decremenet remaining steps, by calling this procedure we make one step
		
					# --- A to P SEGMENT ---		
		beqz	$s1, koch_noap	# jump over recursive call if no more steps remaining
		
		move	$a0, $s4	# Ax
		move	$a1, $s5	# Ay
		lw	$a2, -4($fp)	# Px
		lw	$a3, -8($fp)	# Py
		subiu	$sp, $sp, 8	# make space for arguments
		sw	$s1, 4($sp)	# remaining steps
		sw	$s0, ($sp)	# current address
		jal	koch		# recursive koch curve
		addiu	$sp, $sp, 8	# cleanup stack from arguments
		addu	$s2, $s2, $v1	# sum added points from deeper call
		move	$s0, $v0	# current address to write points
		
					# --- P to Q segment ---
koch_noap:	lw	$a0, -4($fp)	# load Px into $a0
		lw	$a1, -8($fp)	# load Py into $a1	
		
		sw	$a0, ($s0)	# store Px in buffer
		sw	$a1, 4($s0)	# store Py in buffer
		addiu	$s0, $s0, 8	# next address
		
		beqz	$s1, koch_nopq	# jump over recursive call if no more steps remaining
		
		# $a0			# Px, already set
		# $a1			# Py, already set
		lw	$a2, -12($fp)	# Qx
		lw	$a3, -16($fp)	# Qy
		subiu	$sp, $sp, 8	# make space for arguments
		sw	$s1, 4($sp)	# remaining steps
		sw	$s0, ($sp)	# current address
		jal	koch		# recursive koch curve
		addiu	$sp, $sp, 8	# cleanup stack from arguments
		addu	$s2, $s2, $v1	# sum added points from deeper call
		move	$s0, $v0	# current address to write points
		
					# --- Q to R segment ---
koch_nopq:	lw	$a0, -12($fp)	# load Qx into $a0
		lw	$a1, -16($fp)	# load Qy into $a1
		
		sw	$a0, ($s0)	# store Qx in buffer
		sw	$a1, 4($s0)	# store Qy in buffer
		addiu	$s0, $s0, 8	# next address
		
		beqz	$s1, koch_noqr	# jump over recursive call if no more steps remaining
		
		# $a0			# Qx, already set
		# $a1			# Qy, already set
		lw	$a2, -20($fp)	# Rx
		lw	$a3, -24($fp)	# Ry
		subiu	$sp, $sp, 8	# make space for arguments
		sw	$s1, 4($sp)	# remaining steps
		sw	$s0, ($sp)	# current address
		jal	koch		# recursive koch curve
		addiu	$sp, $sp, 8	# cleanup stack from arguments
		addu	$s2, $s2, $v1	# sum added points from deeper call
		move	$s0, $v0	# current address to write points
		
					# --- R to B segment
koch_noqr:	lw	$a0, -20($fp)	# load Rx into $a0
		lw	$a1, -24($fp)	# load Ry into $a1
		
		sw	$a0, ($s0)	# store Rx in buffer
		sw	$a1, 4($s0)	# store Ry in buffer
		addiu	$s0, $s0, 8	# next address
		
		beqz	$s1, koch_norb	# jump over recursive call if no more steps remaining
		
		# $a0			# Rx, already set
		# $a1			# Ry, already set
		lw	$a2, -28($fp)	# Bx
		lw	$a3, -32($fp)	# By
		subiu	$sp, $sp, 8	# make space for arguments
		sw	$s1, 4($sp)	# remaining steps
		sw	$s0, ($sp)	# current address
		jal	koch		# recursive koch curve
		addiu	$sp, $sp, 8	# cleanup stack from arguments
		addu	$s2, $s2, $v1	# sum added points from deeper call
		move	$s0, $v0	# current address to write points
		
koch_norb:	addiu	$s2, $s2, 3	# added 3 points - P, Q and R
		
					# --- EPILOGUE ---
		move	$v0, $s0	# return current address
		move	$v1, $s2	# return count of added points
		lw	$ra, ($sp)	# restore $ra
		lw	$s0, 4($sp)	# restore $s0
		lw	$s1, 8($sp)	# restore $s1
		lw	$s2, 12($sp)	# restore $s2
		lw	$s3, 16($sp)	# restore $s3
		lw	$s4, 20($sp)	# restore $s4
		lw	$s5, 24($sp)	# restore $s5
		lw	$s6, 28($sp)	# restore $s6
		lw	$s7, 32($sp)	# restore $s7
		lw	$fp, ($fp)	# restore $fp
		addiu	$sp, $sp, 72	# cleanup stack
		
		jr	$ra
