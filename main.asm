.data
inputFile:	.asciiz "C:\\Users\\Szymon\\Repos\\arko_mips\\input.txt"
outputFile:	.asciiz "C:\\Users\\Szymon\\Repos\\arko_mips\\output.bmp"

.align 2
pixelBuffer:	.space 1000000		# max 1000 x 1000 bitmap
coordsBuffer:	.space 128		# max 16 coordinates
resultBuffer:	.space 2621480		# max 5 * 4^8 points

.text
.globl main
main:
					# --- LOAD DATA FROM FILE ---
		la	$a0, inputFile	# address of null-terminated string containing input file name
		la	$a1, coordsBuffer # address of output buffer to hold coordinates
		jal	loadData	# load data from file
		move	$s0, $v0	# hold number of steps in $s0
		move	$s1, $v1	# hold count of coordinates in $s1
		
					# --- CONVERT COORDINATES TO FIXED POINT ---
		la	$a0, coordsBuffer # address of coordinates buffer
		sll	$a1, $s1, 1	# count of numbers, coordinates * 2
		jal	convertToFP	# converts numbers in the buffer to fixed point
		
					# --- KOCH ALGORITHM ---
		la	$a0, coordsBuffer # address of buffer containing coordinates
		move	$a1, $s1	# count of coordinates
		move	$a2, $s0	# number of steps
		la	$a3, resultBuffer # address of output buffer
		jal	fullKoch	# run koch algorithm
		move	$s1, $v0	# hold count of points in $s1
		
					# --- CONVERT TO INTEGER ---
		la	$a0, resultBuffer # address of buffer containing numbers
		sll	$a1, $s1, 1	# count of numbers, points * 2
		jal	convertToInt	# converts numbers in the buffer to integer
			
					# --- ALIGN COORDINATES ---
		la	$a0, resultBuffer # address of buffer with coordinates
		move	$a1, $s1	# count of coordinates
		jal	alignCoords	# align coordinates, compute bounds
		move	$s2, $v0	# $s2 - width
		move	$s3, $v1	# $s3 - height

					# --- CLEAR BITMAP ---
		la	$a0, pixelBuffer # address of buffer
		mul	$a1, $s2, $s3	# size of buffer
		li	$a2, 0x00	# value used to fill up buffer (white color)
		jal	memset
		
					# --- DRAWING COMPUTED KOCH CURVE ---
		la	$a0, resultBuffer # address of buffer with coordinates
		move	$a1, $s1	# count of coordinates
		la	$a2, pixelBuffer # address of grayscale pixel buffer
		move	$a3, $s2	# width of pixel buffer
		jal	drawCurve	# draws curve from given points
		
					# --- SAVE BITMAP TO FILE ---
		la	$a0, outputFile	# addres of string containing file name
		la	$a1, pixelBuffer# address of buffer containing grayscale pixels
		move	$a2, $s2	# width of bitmap
		move	$a3, $s3	# height of bitmap
		jal	saveBmp
					
					# --- EXIT ---
		li 	$v0, 10
		syscall